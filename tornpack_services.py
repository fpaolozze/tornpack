from tornpack.ioengine import IOEngine

def run(service):
    try:
        exec "import %s" % service
    except:
        raise

with open('.services') as services:
    for service in services:
        IOEngine.ioloop.add_callback(run,service=service)
