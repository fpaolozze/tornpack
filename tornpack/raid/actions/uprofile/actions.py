import acl
import corp
import app
import user

__all__ = ['actions']

actions = {
    'acl_delete':acl.delete.Delete().on_msg,
    'acl_get':acl.get.Get().on_msg,
    'acl_group_delete':acl.group.delete.Delete().on_msg,
    'acl_group_get':acl.group.get.Get().on_msg,
    'acl_group_save':acl.group.save.Save().on_msg,
    'acl_set':acl.set.Set().on_msg,
    'app_auth':app.auth.Auth().on_msg,
    'app_create':app.create.Create().on_msg,
    'app_gen_token':app.gen_token.GenToken().on_msg,
    'corp_email_add':corp.email.add.Add().on_msg,
    'corp_email_delete':corp.email.delete.Delete().on_msg,
    'corp_email_get':corp.email.get.Get().on_msg,
    'corp_get':corp.get.Get().on_msg,
    'corp_save':corp.save.Save().on_msg,
    'user_auth':user.auth.Auth().on_msg,
    'user_create':user.create.Create().on_msg,
    'user_gen_token':user.gen_token.GenToken().on_msg
}
