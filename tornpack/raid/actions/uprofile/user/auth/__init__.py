from tornpack.actor.rabbitmq import SimpleAsk
from tornpack.options import options
from tornpack.actor.rabbitmq.static import routing_key
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['Auth']

class Auth(SimpleAsk):
    def app_auth(self,msg,io):
        try:
            assert io.app
        except AssertionError:
            self.__acl_error__(io,msg['header']['etag'])
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(self.user_auth,msg=msg,io=io)
        return True

    def ask(self,**kwargs):
        kwargs = kwargs['msg']

        def on_tell(result):
            try:
                io.push({
                    'body':dictfy(result.result()['body']),
                    'header':{
                        'etag':msg['header']['etag'],
                        'code':result.result()['properties'].headers['code']
                    }
                })
            except KeyError:
                pass
            except:
                raise

            self.ioengine.ioloop.add_callback(
                self.check_result,
                result=result.result(),
                msg=msg,
                io=io
            )
            return True

        self.ioengine.ioloop.add_callback(
            self.__ask__,
            future=self.ioengine.future_instance(on_tell),
            body=jsonify(msg['body']),
            routing_key=routing_key(options.tornpack_uprofile_rabbitmq['services']['user']['auth'])
        )
        return True

    def check_result(self,result,msg,io):
        try:
            assert result['properties'].headers['code'] == options.tornpack_uprofile_rabbitmq['codes']['user']['auth']['ok']
        except AssertionError:
            self.ioengine.ioloop.add_callback(io.disconnect)
        except:
            raise
        else:
            io.user_auth(msg['body']['user'],msg['body']['type'])
        return True

    def on_msg(self,msg,io):
        try:
            assert msg['body']['client_token']
            assert msg['body']['user']
            assert msg['body']['user_token']
            assert msg['body']['type'] in options.tornpack_uprofile_user_types
        except:
            io.error(
                etag=msg['header']['etag'],
                code=options.tornpack_raid_codes['payload']['invalid']
            )
        else:
            self.ioengine.ioloop.add_callback(self.app_auth,msg=msg,io=io)
        return True

    def user_auth(self,msg,io):
        try:
            assert io.user
        except AssertionError:
            self.ioengine.ioloop.add_callback(self.ask,msg=msg,io=io)
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(io.disconnect)
        return True
