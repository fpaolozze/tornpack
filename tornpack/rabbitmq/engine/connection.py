from pika import URLParameters
from pika.adapters.tornado_connection import TornadoConnection
from tornpack.actor.base import Base

__all__ = ['Connection']

class Connection(Base):
    def __open(self,url,future):
        def on_open(connection):
            try:
                future.set_result(connection)
            except AttributeError:
                pass
            except:
                raise
            return True

        def on_open_error(connection,text):
            self.ioengine.ioloop.add_timeout(
                self.ioengine.ioloop.time() + 5,
                self.__open,
                url=url,
                future=future
            )
            return True

        TornadoConnection(
            url,
            on_open_callback=on_open,
            on_open_error_callback=on_open_error,
            stop_ioloop_on_close=False
        )
        return True

    def __url_parse(self,url,future):
        try:
            self.ioengine.ioloop.add_callback(
                self.__open,
                url=URLParameters(url),
                future=future
            )
        except:
            raise
        return True

    def make(self,**kwargs):
        try:
            assert kwargs['msg']['url']
        except (AssertionError,KeyError):
            pass
        except:
            raise
        else:
            self.ioengine.ioloop.add_callback(
                self.__url_parse,
                url=kwargs['msg']['url'],
                future=kwargs.get('future')
            )
        return True

Connection = Connection()
