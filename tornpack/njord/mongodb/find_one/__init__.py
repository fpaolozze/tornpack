from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mongodb.static import mongodb_path as MDBPATH
from tornpack.options import options
from tornpack.parser.json import dictfy,jsonify

__all__ = ['FindOne']

class FindOne(Base):
    @property
    def name(self):
        return 'njord_mongodb_find_one'

    @property
    def service(self):
        return 'find_one'

    def mongodb_path(self,env,db,collection):
        return MDBPATH(env,db,collection,self.service)

    def run(self,env,db,collection,query,future,fields=None,skip=0,sort=None):
        def on_ask(result):
            future.set_result(dictfy(result.result()['body']))
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            body=query,
            headers={
                'mongodb_path':self.mongodb_path(
                    env=env,
                    db=db,
                    collection=collection
                ),
                'fields':fields,
                'skip':skip,
                'sort':sort
            }
        )
        return True
