from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.njord.mongodb.static import mongodb_path as MDBPATH
from tornpack.options import options
from tornpack.parser.json import dictfy

__all__ = ['Insert']

class Insert(Base):
    @property
    def name(self):
        return 'njord_mongodb_insert'

    @property
    def service(self):
        return 'insert'

    def mongodb_path(self,env,db,collection):
        return MDBPATH(env,db,collection,self.service)

    def run(self,env,db,collection,payload,future):
        def on_ask(result):
            future.set_result(dictfy(result.result()['body']))
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            body=payload,
            headers={
                'mongodb_path':self.mongodb_path(
                    env=env,
                    db=db,
                    collection=collection
                )
            }
        )
        return True
