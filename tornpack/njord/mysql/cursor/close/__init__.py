from tornpack.actor.base import Base
from tornpack.njord import Njord
from tornpack.options import options

__all__ = ['Close']

class Close(Base):
    @property
    def name(self):
        return 'njord_mysql_cursor_close'

    @property
    def service(self):
        return 'cursor_close'

    def run(self,service,cursor,future=None):
        def on_ask(result):
            try:
                future.set_result(True)
            except AttributeError:
                pass
            except:
                raise
            return True

        Njord.publish(
            name=self.name,
            future=self.ioengine.future_instance(on_ask),
            headers={
                'service':service,
                'action':self.name,
                'cursor':cursor
            }
        )
        return True
