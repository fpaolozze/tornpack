from tornpack.njord.mysql.cursor.close import Close
from tornpack.njord.mysql.cursor.fetch import Fetch

__all__ = ['Handler']

class Handler(object):
    __closed = False
    __cursor = None
    __service = None

    @property
    def closed(self):
        return self.__closed

    @property
    def cursor(self):
        return self.__cursor

    @property
    def service(self):
        return self.__service

    def __cursor__(self):
        return {
            'service':self.service,
            'cursor':self.cursor
        }

    def __init__(self,service,cursor):
        self.__service = service
        self.__cursor = cursor

    def close(self,future=None):
        try:
            assert not self.closed
        except AssertionError:
            return False
        except:
            raise

        self.__closed = True
        Close().run(
            service=self.service,
            cursor=self.cursor,
            future=future
        )
        return True

    def next(self,future):
        try:
            assert not self.closed
        except AssertionError:
            future.set_exception(StopIteration())
            return False
        except:
            raise

        Fetch().run(
            service=self.service,
            cursor=self.cursor,
            future=future
        )
        return True
