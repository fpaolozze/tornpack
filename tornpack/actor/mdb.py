from .base import Base
from hashlib import sha1
from tornpack.mongodb import Mongodb

__all__ = ['MDB']

class MDB(Base):
    __dbs = {}

    def __db_hash(self,name,db,collection):
        return sha1('mongodb[%s][%s][%s]' % (name,db,collection)).hexdigest()
    
    def db(self,name,db,collection,future):
        def on_client(result):
            self.__dbs[self.__db_hash(name,db,collection)] = result.result()
            future.set_result(result.result())
            return True
        
        def on_engine(result):
            result.result().client(db,collection,future=self.ioengine.future_instance(on_client))
            return True
        
        try:
            assert self.__dbs[self.__db_hash(name,db,collection)]
        except (AssertionError,KeyError,TypeError):
            Mongodb.engine(name,future=self.ioengine.future_instance(on_engine))
        except:
            raise
        else:
            future.set_result(self.__dbs[self.__db_hash(name,db,collection)])
        return True
