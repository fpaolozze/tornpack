from tornpack.actor.rabbitmq.channel import Channel
from tornpack.actor.rabbitmq.connection import Connection

__all__ = ['SimpleBinder']

class SimpleBinder(Channel,Connection):
    def __init__(self):
        self.name = self.uid
        self.ioengine.ioloop.add_callback(self.__on_init__)
        self.ioengine.ioloop.add_callback(self.__rabbitmq_connection__)

    def __on_init__(self):
        pass

    def __rabbitmq_queue_bind__(self,queue,exchange,future,routing_key='',headers={}):
        try:
            assert self.rabbitmq_connection
        except AssertionError:
            def on_conn(conn):
                self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_bind__,
                                                  queue=queue,
                                                  exchange=exchange,
                                                  future=future,
                                                  routing_key=routing_key,
                                                  headers=headers)
                return True

            self.__rabbitmq_connection__(self.ioengine.future_instance(on_conn))
        except:
            raise
        else:
            def on_channel(channel):
                def on_bind(result):
                    self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
                    return True

                channel.result().queue_bind(queue=queue,
                                            exchange=exchange,
                                            routing_key=routing_key,
                                            arguments=headers,
                                            callback=on_bind)
                return True

            self.__rabbitmq_channel__(self.ioengine.future_instance(on_channel))
        return True

    def __rabbitmq_queue_unbind__(self,queue,exchange,future,routing_key='',headers={}):
        try:
            assert self.rabbitmq_connection
        except AssertionError:
            def on_conn(conn):
                self.ioengine.ioloop.add_callback(self.__rabbitmq_queue_unbind__,
                                                  queue=queue,
                                                  exchange=exchange,
                                                  future=future,
                                                  routing_key=routing_key,
                                                  headers=headers)
                return True

            self.__rabbitmq_connection__(self.ioengine.future_instance(on_conn))
        except:
            raise
        else:
            def on_channel(channel):
                def on_unbind(result):
                    self.ioengine.ioloop.add_callback(self.future,msg={'future':future,'swap':{'future':True}})
                    return True

                channel.result().queue_unbind(queue=queue,
                                              exchange=exchange,
                                              routing_key=routing_key,
                                              arguments=headers,callback=on_unbind)
                return True

            self.__rabbitmq_channel__(self.ioengine.future_instance(on_channel))
        return True
