from hashlib import sha1
from tornado.web import asynchronous as async,RequestHandler
from tornpack.actor.base import Base
from kernel import Kernel

__all__ = ['async','HTTP']

class HTTP(RequestHandler,Base):
    __etag = None

    @property
    def etag(self):
        try:
            assert self.__etag
        except AssertionError:
            self.__etag = sha1('%s<%s>' % (self.ioengine.uuid4,self.ioengine.uuid4)).hexdigest()
        except:
            raise
        return self.__etag

    @property
    def kernel(self):
        return Kernel

    def __on_finish__(self):
        pass

    def __prepare__(self):
        pass

    def on_finish(self):
        self.kernel.connection_del(self)
        self.ioengine.ioloop.add_callback(self.__on_finish__)

    def prepare(self):
        self.kernel.connection_add(self)
        self.ioengine.ioloop.add_callback(self.__prepare__)
