import jwt
from jwt.exceptions import DecodeError
from tornpack.options import options

__all__ = [
    'decode',
    'DecodeError',
    'encode'
]

def decode(payload,secret):
    return jwt.decode(payload,secret,algorithms=['HS256'])

def encode(payload,secret):
    return jwt.encode(payload,secret,algorithm='HS256')
