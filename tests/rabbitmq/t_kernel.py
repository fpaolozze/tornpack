from tornado import gen
from tornado.concurrent import Future
from tornpack.ioengine import IOEngine
from tornpack.rabbitmq import RabbitMQ
from tornpack.rabbitmq.engines import ENGINES
from tornpack.testing import AsyncTestCase

__all__ = ['TestKernel']

class TestKernel(AsyncTestCase):
    def test_engine_close(self):
        future_close = Future()

        class MockEngine(object):
            def close(self):
                future_close.set_result(True)

            def connection(self,future):
                future.set_result(self)

        RabbitMQ.__engine__ = MockEngine
        ENGINES['test'] = MockEngine()

        def on_future(result):
            IOEngine.ioloop.stop()
            self.assertTrue(future_close.result())
            self.assertFalse('test' in ENGINES)

        RabbitMQ.engine_close('test',IOEngine.future_instance(on_future))
        IOEngine.ioloop.start()

    def test_engine_for_first_time(self):
        class MockEngine(object):
            def on_close(self,future):
                pass

            def connection(self,future):
                future.set_result(True)

            def run(self,name,url):
                pass

        RabbitMQ.__engine__ = MockEngine

        def on_future(result):
            IOEngine.ioloop.stop()
            self.assertTrue('test' in ENGINES)
            del ENGINES['test']

        RabbitMQ.engine('test',IOEngine.future_instance(on_future))
        IOEngine.ioloop.start()

    def test_engine_for_second_time(self):
        class MockEngine(object):
            def on_close(self,future):
                pass

            def connection(self,future):
                future.set_result(True)

            def run(self,name,url):
                pass

        obj = MockEngine()
        RabbitMQ.__engine__ = MockEngine
        ENGINES = {'test':obj}

        def on_future(result):
            IOEngine.ioloop.stop()
            self.assertTrue(obj,ENGINES['test'])
            del ENGINES['test']

        RabbitMQ.engine('test',IOEngine.future_instance(on_future))
        IOEngine.ioloop.start()

    def test_engine_open_for_first_time(self):
        future_connection = Future()
        future_run = Future()

        class MockEngine(object):
            def on_close(self,future):
                pass

            def connection(self,future):
                future.set_result(True)
                future_connection.set_result(True)

            def run(self,name,url):
                future_run.set_result(True)

        RabbitMQ.__engine__ = MockEngine
        ENGINES = {}

        @gen.engine
        def on_future(result):
            IOEngine.ioloop.stop()
            yield future_connection
            yield future_run
            del ENGINES['test']

        RabbitMQ.engine_open('test','url',IOEngine.future_instance(on_future))
        IOEngine.ioloop.start()

    def test_engine_open_for_second_time(self):
        class MockEngine(object):
            def on_close(self,future):
                pass

            def connection(self,future):
                future.set_result(True)

            def run(self,name,url):
                del ENGINES['test']

        obj = MockEngine()
        RabbitMQ.__engine__ = MockEngine
        ENGINES = {'test':obj}

        def on_future(result):
            IOEngine.ioloop.stop()
            self.assertEquals(obj,ENGINES['test'])

        RabbitMQ.engine_open('test','url',IOEngine.future_instance(on_future))
        IOEngine.ioloop.start()
