from tornpack.ioengine import IOEngine
from tornpack.njord.mysql.exceptions import DecodeException,EncodeException,QueryInvalid
from tornpack.njord.mysql.insert import Insert
from tornpack.njord.mysql.insert.insert_pb2 import Insert as PBInsert
from tornpack.njord.mysql.static import mysql_path
from tornpack.parser.json import jsonify
from tornpack.testing import AsyncTestCase

__all__ = ['TestInsert']

class TestInsert(AsyncTestCase):
    def test__op_decode_error__(self):
        def on_call(result):
            try:
                result.result()
            except DecodeException:
                IOEngine.ioloop.stop()

        Insert().__op_decode_error__(None,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_decode_error__without_data(self):
        self.assertRaises(TypeError,Insert().__op_decode_error__,future=None)

    def test__op_decode_error__without_future(self):
        self.assertRaises(TypeError,Insert().__op_decode_error__,data=None)

    def test__op_nok__(self):
        def on_call(result):
            self.assertFalse(result.result())
            IOEngine.ioloop.stop()

        Insert().__op_nok__(None,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_nok__data(self):
        self.assertRaises(TypeError,Insert().__op_nok__,future=None)

    def test__op_nok__future(self):
        self.assertRaises(TypeError,Insert().__op_nok__,data=None)

    def test__op_ok__(self):
        json = {'a':2}
        def on_call(result):
            self.assertEquals(json,result.result())
            IOEngine.ioloop.stop()

        Insert().__op_ok__(jsonify(json),IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_ok__data(self):
        self.assertRaises(TypeError,Insert().__op_ok__,future=None)

    def test__op_ok__future(self):
        self.assertRaises(TypeError,Insert().__op_ok__,data=None)

    def test__op_query_error__(self):
        def on_call(result):
            try:
                result.result()
            except QueryInvalid:
                IOEngine.ioloop.stop()

        Insert().__op_query_error__(None,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_query_error__data(self):
        self.assertRaises(TypeError,Insert().__op_query_error__,future=None)

    def test__op_query_error__future(self):
        self.assertRaises(TypeError,Insert().__op_query_error__,data=None)

    def test_etag(self):
        obj = Insert()
        self.assertEquals(obj.etag,obj.etag)

    def test_mysql_path(self):
        obj = Insert()
        self.assertEquals(obj.mysql_path('test','dbtest','dbtable'),mysql_path('test','dbtest','dbtable',obj.service))

    def test_mysql_path_without_db(self):
        self.assertRaises(TypeError,Insert().mysql_path,env='test',table='dbtable')

    def test_mysql_path_without_env(self):
        self.assertRaises(TypeError,Insert().mysql_path,db='dbtest',table='dbtable')

    def test_mysql_path_without_table(self):
        self.assertRaises(TypeError,Insert().mysql_path,env='test',db='dbtest')

    def test_name(self):
        self.assertEquals('njord_mysql_insert',Insert().name)

    def test_service(self):
        self.assertEquals('insert',Insert().service)
