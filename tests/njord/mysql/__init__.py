from t_cursor_close import TestCursorClose
from t_cursor_fetch import TestCursorFetch
from t_cursor_handler import TestCursorHandler
from t_delete import TestDelete
from t_insert import TestInsert
from t_static import TestStatic
from t_select import TestSelect
from t_select_one import TestSelectOne
from t_update import TestUpdate

__all__ = [
    'TestCursorClose',
    'TestCursorFetch',
    'TestCursorHandler',
    'TestDelete',
    'TestInsert',
    'TestStatic',
    'TestSelect'
    'TestSelectOne',
    'TestUpdate'
]
