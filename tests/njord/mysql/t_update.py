from tornpack.ioengine import IOEngine
from tornpack.njord.mysql.exceptions import DecodeException,QueryInvalid
from tornpack.njord.mysql.update import Update
from tornpack.testing import AsyncTestCase

__all__ = ['TestUpdate']

class TestUpdate(AsyncTestCase):
    def test__op_decode_error__(self):
        def on_call(result):
            try:
                result.result()
            except DecodeException:
                IOEngine.ioloop.stop()

        Update().__op_decode_error__(IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_decode_error__without_future(self):
        self.assertRaises(TypeError,Update().__op_decode_error__)

    def test__op_nok__(self):
        def on_call(result):
            self.assertFalse(result.result())
            IOEngine.ioloop.stop()

        Update().__op_nok__(IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_nok__without_future(self):
        self.assertRaises(TypeError,Update().__op_nok__)

    def test__op_ok__(self):
        json = {'a':2}
        def on_call(result):
            self.assertTrue(result.result())
            IOEngine.ioloop.stop()

        Update().__op_ok__(IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_ok__without_future(self):
        self.assertRaises(TypeError,Update().__op_ok__)

    def test__op_query_error__(self):
        def on_call(result):
            try:
                result.result()
            except QueryInvalid:
                IOEngine.ioloop.stop()

        Update().__op_query_error__(IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_query_error__without_future(self):
        self.assertRaises(TypeError,Update().__op_query_error__)

    def test_etag(self):
        obj = Update()
        self.assertEquals(obj.etag,obj.etag)

    def test_mysql_path_without_db(self):
        self.assertRaises(TypeError,Update().mysql_path,env='test',table='dbtable')

    def test_mysql_path_without_env(self):
        self.assertRaises(TypeError,Update().mysql_path,db='dbtest',table='dbtable')

    def test_mysql_path_without_table(self):
        self.assertRaises(TypeError,Update().mysql_path,env='test',db='dbtest')

    def test_name(self):
        self.assertEquals('njord_mysql_update',Update().name)

    def test_service(self):
        self.assertEquals('update',Update().service)
