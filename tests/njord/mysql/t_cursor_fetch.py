from tornpack.ioengine import IOEngine
from tornpack.njord.mysql.exceptions import DecodeException,EncodeException,QueryInvalid
from tornpack.njord.mysql.cursor.fetch import Fetch
from tornpack.njord.mysql.static import mysql_path
from tornpack.parser.json import jsonify
from tornpack.testing import AsyncTestCase

__all__ = ['TestCursorFetch']

class TestCursorFetch(AsyncTestCase):
    def test__empty__(self):
        def on_call(result):
            IOEngine.ioloop.stop()
            self.assertRaises(StopIteration,result.result)

        Fetch().__empty__(None,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__empty__without_data(self):
        self.assertRaises(TypeError,Fetch().__empty__,future=None)

    def test__empty__without_future(self):
        self.assertRaises(TypeError,Fetch().__empty__,data=None)

    def test__nok__(self):
        def on_call(result):
            IOEngine.ioloop.stop()
            self.assertFalse(result.result())

        Fetch().__nok__(None,IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__nok__without_data(self):
        self.assertRaises(TypeError,Fetch().__nok__,future=None)

    def test__nok__without_future(self):
        self.assertRaises(TypeError,Fetch().__nok__,data=None)

    def test__ok__(self):
        json = {'a':2}
        def on_call(result):
            IOEngine.ioloop.stop()
            self.assertEquals(json,result.result())

        Fetch().__ok__(jsonify(json),IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__ok__without_data(self):
        self.assertRaises(TypeError,Fetch().__ok__,future=None)

    def test__ok__without_future(self):
        self.assertRaises(TypeError,Fetch().__ok__,data=None)

    def test_name(self):
        self.assertEquals('njord_mysql_cursor_fetch',Fetch().name)

    def test_service(self):
        self.assertEquals('cursor_fetch',Fetch().service)
