from tornpack.ioengine import IOEngine
from tornpack.njord import Njord
from tornpack.njord.mysql.exceptions import DecodeException,EncodeException,QueryInvalid
from tornpack.njord.mysql.delete import Delete
from tornpack.njord.mysql.delete.delete_pb2 import Delete as PBDelete
from tornpack.njord.mysql.static import mysql_path
from tornpack.parser.json import jsonify
from tornpack.testing import AsyncTestCase

__all__ = ['TestDelete']

class TestDelete(AsyncTestCase):
    def test__op_decode_error__(self):
        def on_call(result):
            try:
                result.result()
            except DecodeException:
                IOEngine.ioloop.stop()

        Delete().__op_decode_error__(IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_decode_error__without_future(self):
        self.assertRaises(TypeError,Delete().__op_decode_error__)

    def test__op_nok__(self):
        def on_call(result):
            self.assertFalse(result.result())
            IOEngine.ioloop.stop()

        Delete().__op_nok__(IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_nok__without_future(self):
        self.assertRaises(TypeError,Delete().__op_nok__)

    def test__op_ok__(self):
        def on_call(result):
            self.assertTrue(result.result())
            IOEngine.ioloop.stop()

        Delete().__op_ok__(IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_ok__without_future(self):
        self.assertRaises(TypeError,Delete().__op_ok__)

    def test__op_query_error__(self):
        def on_call(result):
            try:
                result.result()
            except QueryInvalid:
                IOEngine.ioloop.stop()

        Delete().__op_query_error__(IOEngine.future_instance(on_call))
        IOEngine.ioloop.start()

    def test__op_query_error__without_future(self):
        self.assertRaises(TypeError,Delete().__op_query_error__)

    def test_etag(self):
        obj = Delete()
        self.assertEquals(obj.etag,obj.etag)

    def test_mysql_path(self):
        obj = Delete()
        self.assertEquals(obj.mysql_path('test','dbtest','dbtable'),mysql_path('test','dbtest','dbtable',obj.service))

    def test_mysql_path_without_db(self):
        self.assertRaises(TypeError,Delete().mysql_path,env='test',table='dbtable')

    def test_mysql_path_without_env(self):
        self.assertRaises(TypeError,Delete().mysql_path,db='dbtest',table='dbtable')

    def test_mysql_path_without_table(self):
        self.assertRaises(TypeError,Delete().mysql_path,env='test',db='dbtest')

    def test_name(self):
        self.assertEquals('njord_mysql_delete',Delete().name)

    def test_service(self):
        self.assertEquals('delete',Delete().service)
