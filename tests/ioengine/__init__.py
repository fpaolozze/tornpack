from tornpack.ioengine import IOEngine
from tornpack.testing import AsyncTestCase

__all__ = ['TestIOEngine']

class TestIOEngine(AsyncTestCase):
    def test_routines_null(self):
        self.assertFalse(IOEngine.routines)
    
    def test_ioloop(self):
        self.assertTrue(IOEngine.ioloop)
    
    def test_uuid(self):
        self.assertNotEquals(IOEngine.uuid4,IOEngine.uuid4)
