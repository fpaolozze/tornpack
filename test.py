from tornpack.options import define

define('testing',default=True)

import config
from tests import tests
import tornpack_settings
from tornpack.testing import main, unittest

def all():
    with open('.services') as services:
        for service in services:
            try:
                exec "from %s.tests import suite" % service.rstrip()
                tests.extend(suite)
            except ImportError:
                pass
            except:
                raise

    return unittest.defaultTestLoader.loadTestsFromNames(tests)

main(verbosity=2)
